Param($dir)
Push-Location $dir
Get-ChildItem | Sort-Object LastWriteTime -Descending | Select-Object -Skip 4 | Remove-Item
Pop-Location