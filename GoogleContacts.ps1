﻿Param($userName, $passWord)
Add-Type -Path 'C:\Program Files\Google\Google Data API SDK\Redist\Google.GData.Contacts.dll'
Add-Type -Path 'C:\Program Files\Google\Google Data API SDK\Redist\Google.GData.Client.dll'
Add-Type -Path 'C:\Program Files\Google\Google Data API SDK\Redist\Google.GData.Extensions.dll'

$reqSet = New-Object Google.GData.Client.RequestSettings("Name",$userName, $passWord)
$contReq = New-Object Google.Contacts.ContactsRequest($reqSet)

$res = $contReq.GetContacts()
#
$res.Entries | foreach {$_.name}