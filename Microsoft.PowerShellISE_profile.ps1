$ScriptDir = "c:\Users\Morten\Mercurial\powershellscripts\"
$MercurialDir = "D:\HG\"
$DownloadDir = "c:\Users\Morten\Downloads\"

. "C:\Users\Morten\Documents\WindowsPowerShell\Modules\profile.example.ps1"
. 'c:\Users\Morten\Documents\WindowsPowerShell\Modules\ENV-Modify-Module.ps1'

$env:path = $env:path + ";$ScriptDir"

[System.Enum]::GetNames([System.Environment+SpecialFolder]) | ForEach-Object{New-PSDrive -Name ($_) -Root([System.Environment]::GetFolderPath($_)) -PSProvider FileSystem}
New-PSDrive -Name ("MyBitBucket") -Root($MercurialDir) -PSProvider FileSystem
New-PSDrive -Name("MyDownloads") -Root($DownloadDir) -PSProvider FileSystem

# Set up a simple prompt, adding the hg prompt parts inside hg repos
function prompt {
    $loc = $(get-location).ToString()
    $usr = ($env:userprofile).ToString()
    
	if ($loc.StartsWith($usr, $true,[System.Globalization.CultureInfo].CurrentCulture)) {
		$loc = "~"+$loc.SubString($usr.Length, $loc.Length - $usr.Length)
    }
	
	Write-Host("PS " + $loc +">") -nonewline
        
    # Mercurial Prompt
    $Global:HgStatus = Get-HgStatus
    Write-HgStatus $HgStatus
      
    return "> "
}

function Out-ReportExcel {
 param(
  $Path = "$env:temp\report$(Get-Date -format yyyyMMddHHmmss).csv",
  [switch]$Open
 )

 $Input | 
  Export-Csv $Path -NoTypeInformation -UseCulture -Encoding UTF8

  if($Open) { Invoke-Item $Path }
}

function Measure-Loc
{
param()
begin
{}
process
{
  (dir -include *.cs -recurse | select-string "^(\s*)//" -notMatch | select-string "^(\s*)$" -notMatch).Count
}
end
{}
}

function Get-NewInDir{
[CmdletBinding()]
param(
	[Parameter(Mandatory=$True,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)]
    $SourcDirectory = $null,
	[Parameter(Mandatory=$True,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)]
    $DestinationDirectory = $null
)
BEGIN {
}
PROCESS {
    $sourceElements = Get-ChildItem -Path $SourcDirectory
    $destinationElements = Get-ChildItem -Path $DesSourcDirectory
	$results = Compare-Object $sourceElements $destinationElements | Where-Object{$_.SideIndicator -eq '<='}
	
	foreach($result in $results){
		Write-Output $result.InputObject
	}
}

END {
}
}

function Copy-NewFilesInDir{
[CmdletBinding()]
param(
	[Parameter(Mandatory=$True,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)]
    $SourcDirectory = $null,
	[Parameter(Mandatory=$True,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)]
    $DestinationDirectory = $null
)
BEGIN {
}
PROCESS {
	Get-NewInDir $SourcDirectory $DestinationDirectory | Copy-Item -Destination $DestinationDirectory
}

END {
}
}

function Validate-Xml {
param(
    $InputObject = $null,
    $Namespace = $null,
    $SchemaFile = $null
)
BEGIN {
    $failCount = 0
    $failureMessages = ""
    $fileName = ""
}
PROCESS {
    if ($InputObject -and $_) {
        throw 'ParameterBinderStrings\AmbiguousParameterSet'
        break
    } elseif(!$SchemaFile) {
		$SchemaFile = Get-ChildItem *.xsd | Select-Object -First 1
		Write-Host($SchemaFile.FullName)
	} elseif ($InputObject) {
        $InputObject
    } elseif ($_) {
        $fileName = $_.FullName
        $readerSettings = New-Object -TypeName System.Xml.XmlReaderSettings
        $readerSettings.ValidationType = [System.Xml.ValidationType]::Schema
        $readerSettings.ValidationFlags = [System.Xml.Schema.XmlSchemaValidationFlags]::ProcessInlineSchema -bor
            [System.Xml.Schema.XmlSchemaValidationFlags]::ProcessSchemaLocation -bor 
            [System.Xml.Schema.XmlSchemaValidationFlags]::ReportValidationWarnings
        $readerSettings.Schemas.Add($Namespace, $SchemaFile) | Out-Null
        $readerSettings.add_ValidationEventHandler(
        {
            $failureMessages = $failureMessages + [System.Environment]::NewLine + $fileName + " - " + $_.Message
            $failCount = $failCount + 1
        });
        $reader = [System.Xml.XmlReader]::Create($_, $readerSettings)
        while ($reader.Read()) { }
        $reader.Close()
    } else {
        throw 'ParameterBinderStrings\InputObjectNotBound'
    }
}

END {
    $failureMessages
    "$failCount validation errors were found"
}
}

####
#Move Each file to itown Dir with same name as Filename!
####
function move-FilesToSeperateDir{
	param(
		[Parameter(Mandatory=$True,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)]
	    $SourcDirectory = ".\",
		[Parameter(Mandatory=$True,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)]
	    $DestinationDirectory = $null
	)
	
	foreach($file in dir $SourcDirectory -File){
		$newPath = Join-Path $DestinationDirectory $file.Name
		New-Item -Path $newPath -type directory 
		Copy-Item $file -Destination $newPath
	}
}