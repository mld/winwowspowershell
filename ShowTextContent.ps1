param($File,$StartStr, $EndStr)
$startLn = Select-String $File -pattern $StartStr | Select-Object LineNumber
$endLn = Select-String $File -pattern $EndStr | Select-Object LineNumber

(Get-Content $File)[($startLn[$startLn.count - 1].LineNumber -1)..($endLn[$endLn.Count - 1].LineNumber - 1)]
