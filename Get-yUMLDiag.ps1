﻿Function Get-yUMLDiagram {
    param(
        $yUML, 
        $diagramFileName="c:\slet\test.jpg",
        [switch]$show,
        [switch]$pdf
    )
    
    $base = "http://yuml.me/diagram/class/"
    $address = $base + $yUML
    [Taglib.File] | Get-Member
    
    if($show) { 
        $wc = New-Object Net.WebClient
        $wc.DownloadFile($address, $diagramFileName)
        Invoke-Item $diagramFileName 
    } elseif ($pdf) {
        $pdfFileName="c:\test.pdf"
        $wc = New-Object Net.WebClient        
        $wc.DownloadFile("$($address).pdf", $pdfFileName)
        Invoke-Item $pdfFileName 

    } else {
        $address
    }
}