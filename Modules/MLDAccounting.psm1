﻿$excel_file_path = 'C:\\Users\\MLD\\Google Drev\\\RegnskabFra07-2015.xlsx'

function AccountingMove-Food{
    param(
        [Parameter(Mandatory)]
    
        [ValidateSet('Tandlæge/kirupraktor etc','Medlemskaber:','Starinlys(fra.10.2016)','Håndsrækning(Rengøring):','Telmore:','Klipning:','Sær-udgifter:(eks. Bil-syn...)','Benzin:','Bil_udgifter:','Parkering','Tøj(NyFraSep.2000)','Kaffe:','Cola_&_Sodavand:','Mad.','Chips,video,Junk-food,slik','Vin(ny fra aug. 2000)','Byturer,fredagsbar,øl,sprut…','Middagsmad/rundstykker... på uni.','Kontorartikler','DVD-Film+Bio+Bøger:','Gaver (Ditte)','Gaver…','Tilgodehavender: (Blå eq.','Tilbagebetalt!)','Afbetaling af gæld:','Ture','Toiletting,køkkenrulle,Støvsugeposer,Sugerør','Emil/Anton:','Værktøj,Skruer,Batterier_olgn.','Have,Blomster,Gødning_osv.','Bolig-ting:','Bolig vedligehold:','Køkkenting(HW)','Medizin:','Software:','Småt_IT_Kabler_etc','Markedførring_af_sparket:','Størr','_ting:','Kortspil :( Fra feb 2002 også Lotto!)','Misc: Poser…')]
        [String] $acc,
        [Double] $amount,
        [Int] $month = 0
        ) 

    $ExcelObj = openWorkSheet
    ##Find Coloumn
    $dateStr = (Get-Date).AddMonths($month).ToString("01-MM-yyyy")
    $column = $ExcelObj.ExcelWorkSheet.Range("B3:IB3").find($dateStr)
    ####Find row

    ## First find the row that is empty
    $LC = get-LastUsedCell($ExcelObj.ExcelWorkSheet)
    $LCRow = $LC.row
    ##Find row with Account
    $row = $ExcelObj.ExcelWorkSheet.Range("A1:A$LCRow").find("$acc",[Type]::Missing,[Type]::Missing,1).row
    $rowMad = $ExcelObj.ExcelWorkSheet.Range("A1:A$LCRow").find('Mad.').row

    ##Add amount!!
    $oldValue = $ExcelObj.ExcelWorkSheet.Cells.Item($rowMad,$column.COLUMN).text
    $formula = $ExcelObj.ExcelWorkSheet.Cells.Item($rowMad,$column.COLUMN).FormulaR1C1 + "-" +($amount/2)
    $ExcelObj.ExcelWorkSheet.Cells.Item($rowMad,$column.COLUMN).FormulaR1C1 = $formula
    Write-host "Mad: " $oldValue "==>" $ExcelObj.ExcelWorkSheet.Cells.Item($rowMad,$column.COLUMN).text

    $oldValueAcc = $ExcelObj.ExcelWorkSheet.Cells.Item($row,$column.COLUMN).text
    $formula = $ExcelObj.ExcelWorkSheet.Cells.Item($row,$column.COLUMN).FormulaR1C1 + "+" +($amount/2)
    $ExcelObj.ExcelWorkSheet.Cells.Item($row,$column.COLUMN).FormulaR1C1 = $formula
    Write-host $acc $oldValueAcc "==>" $ExcelObj.ExcelWorkSheet.Cells.Item($row,$column.COLUMN).text
    
    close-Excel($ExcelObj)
}

function Accounting-Backup () { 
    Copy-Item $excel_file_path (Join-Path 'c:\Users\MLD\Google Drev\RegnskabBackup\' ("RegnskabBackup-" + (Get-Date -f hhmmss-dd-mmm-yyyy).ToString() + ".xlsx")).ToString()
}

function openWorkSheet {
    # Instantiate the Excel COM object
    $Excel = New-Object -ComObject Excel.Application
    $ExcelWorkBook = $Excel.Workbooks.Open($excel_file_path)
    $ExcelWorkSheet = $Excel.WorkSheets.item("Regnskab-2000")
    $ExcelWorkSheet.activate()
    $excelObj = @{
        "excel_file_path" = $excel_file_path;
        "Excel" = $Excel;
        "ExcelWorkBook" =$ExcelWorkBook;
        "ExcelWorkSheet" = $ExcelWorkSheet
      }
      
      $system = New-Object -TypeName PSObject -Property $excelObj
      
   
    return $excelObj  
}
function AccountingSet-Spending {
param(
    [Parameter(Mandatory)]

    [ValidateSet('Tandlæge/kirupraktor etc','Medlemskaber:','Starinlys(fra.10.2016)','Håndsrækning(Rengøring):','Telmore:','Klipning:','Sær-udgifter:(eks. Bil-syn...)','Benzin:','Bil_udgifter:','Parkering','Tøj(NyFraSep.2000)','Kaffe:','Cola_&_Sodavand:','Mad.','Chips,video,Junk-food,slik','Vin(ny fra aug. 2000)','Byturer,fredagsbar,øl,sprut…','Middagsmad/rundstykker... på uni.','Kontorartikler','DVD-Film+Bio+Bøger:','Gaver (Ditte)','Gaver…','Tilgodehavender: (Blå eq.','Tilbagebetalt!)','Afbetaling af gæld:','Ture','Toiletting,køkkenrulle,Støvsugeposer,Sugerør','Emil/Anton:','Værktøj,Skruer,Batterier_olgn.','Have,Blomster,Gødning_osv.','Bolig-ting:','Bolig vedligehold:','Køkkenting(HW)','Medizin:','Software:','Småt_IT_Kabler_etc','Markedførring_af_sparket:','Størr','_ting:','Kortspil :( Fra feb 2002 også Lotto!)','Misc: Poser…')]
    [String] $acc,
    [Double] $amount,
    [Int] $month = 0,
    [bool] $inclWithdraw = $true
    ) 

    $ExcelObj = openWorkSheet

    ##Find Coloumn
    $dateStr = (Get-Date).AddMonths($month).ToString("01-MM-yyyy")
    $column = $ExcelObj.ExcelWorkSheet.Range("B3:IB3").find($dateStr)
    ####Find row

    ## First find the row that is empty
    $LC = get-LastUsedCell($ExcelObj.ExcelWorkSheet)
    $LCRow = $LC.row
    ##Find row with Account
    $row = $ExcelObj.ExcelWorkSheet.Range("A1:A$LCRow").find("$acc").row
    ##Add amount!!
    $oldValue = $ExcelObj.ExcelWorkSheet.Cells.Item($row,$column.COLUMN).text
    $formula = $ExcelObj.ExcelWorkSheet.Cells.Item($row,$column.COLUMN).FormulaR1C1 + "+" +$amount
    $ExcelObj.ExcelWorkSheet.Cells.Item($row,$column.COLUMN).FormulaR1C1 = $formula
    Write-host $acc $oldValue "==>" $ExcelObj.ExcelWorkSheet.Cells.Item($row,$column.COLUMN).text

    if($inclWithdraw){
        $rowHaevning = $ExcelObj.ExcelWorkSheet.Range("A1:A$LCRow").find("Haevning").row
        ##Add amount!!
        $formula = $ExcelObj.ExcelWorkSheet.Cells.Item($rowHaevning,$column.COLUMN).FormulaR1C1 + "+" +$amount
        $ExcelObj.ExcelWorkSheet.Cells.Item($rowHaevning,$column.COLUMN).FormulaR1C1 = $formula
    }

    close-Excel($ExcelObj)
}

function close-Excel ($ExcelObj) {
    $ExcelObj.ExcelWorkBook.Save()
    $ExcelObj.ExcelWorkBook.Close()
    $ExcelObj.Excel.Quit()
    [System.Runtime.Interopservices.Marshal]::ReleaseComObject($ExcelObj.Excel)
    Stop-Process -Name EXCEL -Force
}

function get-LastUsedCell ($ExcelWorkSheet) {
    ##Last used row:
    $xlCellTypeLastCell = 11
    $used = $ExcelWorkSheet.usedRange
    $lastCell = $used.SpecialCells($xlCellTypeLastCell)

    $lastCell
}

Export-ModuleMember AccountingSet-Spending, AccountingMove-Food, Accounting-Backup