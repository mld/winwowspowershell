$dir = 'F:\**\backups'
(New-Object -ComObject WScript.Network).MapNetworkDrive('Y:', "\\Systematic.local\shared\develop\Projects\JSF-OMS-IO\Backups\Atlassian\Confluence\", "true", "Username", "Passwd")
Push-Location $dir
Get-ChildItem |  Move-Item -destination 'Y:'
Get-ChildItem 'Y:' | Sort-Object LastWriteTime -Descending | Select-Object -Skip 10 | Remove-Item
Pop-Location
(New-Object -ComObject WScript.Network).RemoveNetworkDrive('Y:', "True", "True")
