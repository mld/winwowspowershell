﻿############################
## Succesively loops through directories looking for JPGs and mediafiles.
## For each mediafile found in a directory the JPGs found in the same diretory are added as cover art.
##
## NB Only mediafiles whitout any coverart is handled, and only JPGs gt. 50k. are considered!
##    JPGs are added as coverart based on sort by file names!
###########################

$directory = $PWD
$AllPics

$ScriptDirectory = Split-Path $MyInvocation.MyCommand.Path
. (Join-Path $ScriptDirectory ID3TagFunctions.ps1)
[Reflection.Assembly]::LoadFrom( (Resolve-Path "$ScriptDirectory\taglib-sharp.dll") )

function WalkThroughSubDirs
{
param([string]$directory)
	
	if($AllPics){
		$covers = get-AllPics($directory)
		}else{
			$covers = get-Pics($directory)
		
		}
	if($covers)
	{		
		foreach($p in $covers)
		{
			$pic = $pic + @([TagLib.Picture]::CreateFromPath($p.FullName))					
		}
	
		foreach($file in get-MediaFiles($directory))
		{
			$med = [TagLib.File]::Create($file.FullName);
			
			if(!$med.Tag.Pictures)
			{
				$med.Tag.Pictures = $pic
				$med.Save()
			}
			$med = ""			
		}
		$pic = @()
	}
	foreach($f in get-Dirs($directory))
	{
		WalkThroughSubDirs($f.FullName)
	}
}

WalkThroughSubDirs($directory)


