foreach($f in dir -recurse)
{
	if($f.Extension -eq ".MP3") 
	{
		$med = [TagLib.File]::Create($f.FullName);
		$ParentDirLength =(Split-Path -Parent $pwd.Path).Length
		$med.Tag.Album = $pwd.path.Substring($ParentDirLength + 1)
		$med.Save();
	}
}