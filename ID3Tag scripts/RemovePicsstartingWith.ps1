﻿$directory = $PWD

. "C:\Users\MLD\Documents\Repositories\powershellscripts\ID3Tag scripts\ID3TagFunctions.ps1"

function get-AlbumPic{
	param([string]$directory)
	$directory = $directory + "\*"
	return dir $directory  -Include *.JPG, *.jpg -Recurse| Where {$_.Name.StartsWith("AlbumArt")}
}

function RemoveFileStartinWithAlbum
{
param([string]$directory)

	get-AlbumPic($directory) | Remove-Item 
	foreach($f in get-Dirs($directory))
	{
		RemoveFileStartinWithAlbum($f.FullName)
	}
}
if($directory)
{
	RemoveFileStartinWithAlbum($directory)
}