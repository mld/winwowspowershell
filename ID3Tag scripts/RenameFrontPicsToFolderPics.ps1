﻿$directory = $PWD

. (Resolve-Path("C:\Users\MLD\Documents\Repositories\powershellscripts\ID3Tag scripts\ID3TagFunctions.ps1"))

function get-FrontPic{
	param([string]$di)
	return dir $di -Include "*.JPG","*.jpg" | Where {$_.Name.Contains("Front")}
}

function get-FolderPic{
	param([string]$dir1)
		Write-Host "get-FolderPic. dir1:" + $dir1
	
	return dir $dir1 -Include "*.JPG","*.jpg"  | Where {$_.Name.Contains("Folder")}
}

function RenameFrontFile{
	param([string]$d)
	Write-Host "Start RenameFrontFile. dir:" + $d
	
	$FrontPic = ""
	$folderPic =""
	
	$FrontPic = get-FrontPic($d)
	Write-Host "Start RenameFrontFile. FrontPic:" + $FrontPic
	
	if($FrontPic) 
	{
		$folderPic = get-FolderPic($d)
		Write-Host "Start RenameFrontFile. FolderPic:" + $folderPic
		
		if($folderPic){
			$newBackupDir = $d + "/Backup/"
			New-Item $newBackupDir -type directory
			Move-Item -Force -Path $FolderPic -Destination $newBackupDir
			}
	}
	
	if($FrontPic)
	{
		$FrontPic | Rename-Item -NewName "Folder.jpg" 
	}
	$FrontPic = ""
	$folderPic =""

	foreach($f in get-Dirs($d))
	{
		RenameFrontFile($f.FullName)
	}
}

RenameFrontFile($directory)
