﻿$directory = $PWD

$ScriptDirectory = Split-Path $MyInvocation.MyCommand.Path
. (Join-Path $ScriptDirectory ID3TagFunctions.ps1)

function get-FrontPic{
	param([string]$directory)
	return dir $directory  -Include "*.JPG","*.jpg" -Recurse | Where {$_.Name.Contains("Front")}
}

function get-FolderPic{
	param([string]$directory)
	return dir $directory  -Include "*.JPG","*.jpg" -Recurse | Where {$_.Name.Contains("Folder")}
}

function MakeFolderFileBeAddedFirst
{
param([string]$directory)

	foreach($f in get-AllPicsNotFolder($directory))  
	{
$newName  = "Fp " + $f.Name
		rename-Item $f.FullName  $newName
	}
	
	foreach($f in get-Dirs($directory))
	{
		MakeFolderFileBeAddedFirst($f.FullName)
	}
}

