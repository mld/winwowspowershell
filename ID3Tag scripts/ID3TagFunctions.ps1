﻿function get-Dirs
{
 	param([string]$directory)
  	return dir $directory | Where {$_.psIsContainer -eq $true}
}

function get-Pics
{
	param([string]$directory)
	$directory = $directory + "\*"
  	return dir $directory -Include *.jpg, *.JPG| Where {$_.Length -gt 1000} | Sort-Object -Property Name
}

function get-AllPics
{
	param([string]$directory)
	$directory = $directory + "\*"
  	return dir $directory -Include *.JPG, *.jpg 
}

function get-AllPicsNotFolder
{
	param([string]$directory)
	$directory = $directory + "\*"
  	return dir $directory  -Include *.JPG, *.jpg | Where {$_.Name -ne "Folder.jpg"}
}

function get-MediaFiles
{
	param([string]$directory)
	$directory = $directory + "\*"
	return dir $directory -Include *.MP3,*.mp3
}