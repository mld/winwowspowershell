﻿function Out-Excel

{

  param($Path = "$env:temp$(Get-Date -Format yyyyMMddHHmmss).csv")

  $input | Export-CSV -Path $Path -UseCulture -Encoding UTF8 -NoTypeInformation

  Invoke-Item -Path $Path

}

 //Get-Process | Where-Object { $_.MainWindowTitle } | Out-Excel

 $excel = New-Object -ComObject Excel.Application

$excel.Visible = $true

$workbook = $excel.Workbooks.Add()

$sheet = $workbook.ActiveSheet

$counter = 0

Get-Service |

ForEach-Object {

    $counter++

    $sheet.cells.Item($counter,1) = $_.Name

    $sheet.cells.Item($counter,2) = $_.DisplayName

    $sheet.cells.Item($counter,3) = $_.Status

}
